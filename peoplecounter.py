#!/usr/bin/env python3
import time
import requests
import random

last_result_file = "/tmp/kch_lastresult"
max_plato = 30
def_delay = 0.01
upd_delay = 1
inc = 198
reset = 199


def sleep(n = def_delay):
    time.sleep(n)

def init_pins():
    for pn in [reset, inc]:
        try:
            with open("/sys/class/gpio/export", "w") as i:
                print(pn, file=i)
        except BaseException:
            # welp, it was initialized already probably
            pass
        with open(f"/sys/class/gpio/gpio" + str(pn) + "/direction", "w") as i: print("out", file=i)


def blink(pn, delay=0.01):
    with open("/sys/class/gpio/gpio" + str(pn) + "/value", "a") as i:
        print("1", file=i)
        i.flush()
        if delay: sleep(delay / 2)
        print("0", file=i)
        i.flush()
        if delay: sleep(delay / 2)


def quickset(num, delay = 0):
    blink(reset, delay = 0)
    for _ in range(num):
        blink(inc, delay = 0)

def randset(num):
    for _ in range(10):
        quickset(random.randrange(0, 100))
        sleep()
    quickset(num, delay=0.01)

# noinspection PyBroadException
def loadNow():
    try:
        return requests.get('https://api.kocherga.club/people/now').json()["total"]
    except BaseException as a:
        print(a)
        return 99

def saveLast(num):
    with open(last_result_file, "w") as i: print(num, file=i)

def loadLast():
        try:
            with open(last_result_file) as i:
                return int(i.readline())
        except Exception:
            return -1

def main_loop():
    last = loadLast()
    c = 0
    while True:
        now = loadNow()
        if last != now or c <= 0:
            c = max_plato
            randset(now)
            print(now)
            saveLast(now)
            last = now
        time.sleep(upd_delay)
        c -= 1

init_pins();
main_loop();